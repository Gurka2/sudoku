# internal imports
from board import *
from render import clear_terminal, hide_cursor, move_courser_to_pos, render_board_at_pos, render_cursor_pos, show_cursor
from color import Color
import threading

# external imports
from typing import Any

# requirements
import getch

class Game():

    def __init__(self, board_pos_x, board_pos_y):
        self._board_pos_x: int = board_pos_x
        self._board_pos_y: int = board_pos_y
        self._cursor_x: int = 1
        self._cursor_y: int = 1
        self._cursor_color: Color = Color.CYAN
        self._board = Board()
        self._user_input: list[Any] = []
        self._exit: bool = False

    def _calculate_abs_cursor_pos(self,
            cursor_relative_x: int,
            cursor_relative_y: int,
            board_pos:         tuple[int, int]
                                 ) -> tuple[int, int]:

        cursor_padding_x: int = 2
        cursor_padding_y: int = 1

        cursor_scaling_x: int = 4
        cursor_scaling_y: int = 2

        cursor_abs_x: int = cursor_relative_x * cursor_scaling_x + cursor_padding_x + board_pos[0]
        cursor_abs_y: int = cursor_relative_y * cursor_scaling_y + cursor_padding_y + board_pos[1]

        return (cursor_abs_x, cursor_abs_y)

    def _handle_cursor_movement(self, user_input: str):
        """ 
        Moves the cursor around the board according to vim keybinds
        without going outside of the board
        """

        if user_input == 'h':
            self._cursor_x = max(0, self._cursor_x - 1)

        if user_input == 'l':
            self._cursor_x = min(8, self._cursor_x + 1)

        if user_input == 'j':
            self._cursor_y = min(8, self._cursor_y + 1)

        if user_input == 'k':
            self._cursor_y = max(0, self._cursor_y - 1)


    def _take_user_input(self) -> list[str]:
        # crashes on anything but esc, letters and number, e.g. ctrl-d => crash
        self._user_input += getch.getch()

    def _reset_user_input(self):
        self._user_input = []

    def _handle_number_input(self, number: int):
        if self._board.is_legal_value(number, self._cursor_x, self._cursor_y):
            self._board.set_value(number, self._cursor_x, self._cursor_y)

    def _handle_user_input(self):
        user_input = self._user_input[-1]
        
        if user_input in 'qp':
            self._exit = True
        
        if user_input in 'hjkl':
            self._handle_cursor_movement(user_input)

        if user_input in '1234567890':
            self._handle_number_input(int(user_input))

        self._reset_user_input()

    def start_sudoku(self):

        hide_cursor()

        # Runs the game
        while True:

            # Graphics
            clear_terminal()
            render_board_at_pos(self._board, (self._board_pos_x, self._board_pos_y))
            cursor_abs_pos = self._calculate_abs_cursor_pos(
                                                    self._cursor_x,
                                                    self._cursor_y,
                                                    (self._board_pos_x, self._board_pos_y)
                                                    )

            render_cursor_pos(cursor_abs_pos, self._cursor_color)

            self._take_user_input()
            self._handle_user_input()

            if self._exit:
                break
            self._board.solve()

        show_cursor()
        clear_terminal()

    def benchmark(self, file_path):
        file = open(file_path, "r")

        lines = file.readlines()
        for line in lines:
            game, solution = line.split(",")
            self._board.set_boardstate(game)
            self._board.solve()
            solved = self._board.get_str_solution()
            if solved == solution:
                print("Correct")
            else:
                print("-"*20)
                print(f"Expected : {solution}")
                print(f"Got      : {solved}")
                print("-"*20)



