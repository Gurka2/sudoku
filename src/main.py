import sys
from render import *
from board import *
from game import *
from simple_term_menu import TerminalMenu

def main():
    options = ["benchmark", "play", "exit"]
    terminal_menu = TerminalMenu(options)
    menu_entry_index = terminal_menu.show()

    # TODO: option in menu
    _x = 1
    _y = 1

    if len(sys.argv) > 1:
        _x = sys.argv[1]
    if len(sys.argv) > 2:
        _y = sys.argv[2]

    if menu_entry_index == 2:
        return

    elif menu_entry_index == 0:
        file_path = "../tests/sudokutests"
        game = Game(_x, _y)
        game.benchmark(file_path)

    elif menu_entry_index == 1:
        game = Game(_x, _y)
        game.start_sudoku()


        



if __name__ == "__main__":
    main()
