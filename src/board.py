from random import randrange

class Board():
    """
    Whole board
    """

    def __init__(self):
        self._board = [[0 for _ in range(9)] for _ in range(9)]
        self.counter = 0

    def _is_num_in_row(self, value: int, pos_x: int, pos_y: int) -> bool:
        return value in self._board[pos_y]

    def _is_num_in_col(self, value: int, pos_x: int, pos_y: int) -> bool:
        for i in range(9):
            if self._board[i][pos_x] == value:
                return True
        return False

    def _is_num_in_box(self, value: int, pos_x: int, pos_y: int) -> bool:
        box_x = (pos_x//3)*3 # Top left corner of the box
        box_y = (pos_y//3)*3

        for x in range(3):
            for y in range(3):
                if self._board[box_y+y][box_x+x] == value:
                    return True

        return False

    def _get_all_legal_values(self, pos_x, pos_y) -> list[int]:
        existing_values = []

        # Append all values in the row and col
        for value in range(1, 10):
            if self._is_num_in_col(value, pos_x, pos_y) \
                or self._is_num_in_row(value, pos_x, pos_y):
                existing_values.append(value)

        # Append all values in the box
        for x in range(3):
            for y in range(3):
                for value in range(1,10):
                    if self._is_num_in_box(value, pos_x, pos_y):
                        existing_values.append(value)

        return [i for i in range(1,10) if i not in existing_values]

    def is_solved(self) -> bool:
        for i in range(9):
            for value in range(1,10):
                if not self._is_num_in_row(value, i, i):
                    return False
                if not self._is_num_in_col(value, i, i):
                    return False

        for x in range(3):
            for y in range(3):
                for value in range(1,10):
                    if not self._is_num_in_box(value, x, y):
                        return False


        return True

    def solve(self):

        if self.is_solved():
            return

        for y in range(9):
            for x in range(9):
                if self._board[y][x] == 0:

                    for value in self._get_all_legal_values(x, y):
                        self._board[y][x] = value
                        self.solve()
                    self._board[y][x] = 0
                    return

    def get_value(self, x: int, y: int) -> int:
        return self._board[y][x]

    def set_value(self, value: int, pos_x: int, pos_y: int):
        self._board[pos_y][pos_x] = value

    def is_legal_value(self, value: int, pos_x: int, pos_y: int) -> bool:
        """ Checks if a value can be placed in a position """
        return not self._is_num_in_col(value, pos_x, pos_y) \
                and not self._is_num_in_row(value, pos_x, pos_y) \
                and not self._is_num_in_box(value, pos_x, pos_y)

    def set_boardstate(self, state: str):
        for y in range(9):
            for x in range(9):
                value_pos = y*9 + x # The current squares position in the string
                self._board[y][x] = int(state[value_pos])

    def get_str_solution(self) -> str:
        solution = ""
        for y in range(9):
            for x in range(9):
                solution += str(self._board[y][x])
        return solution
